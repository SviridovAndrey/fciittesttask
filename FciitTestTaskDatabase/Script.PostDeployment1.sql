﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

:r .\Scripts\AspNetUsers.sql
:r .\Scripts\Extensions.sql
:r .\Scripts\Books.sql
:r .\Scripts\Authors.sql
:r .\Scripts\BookAuthors.sql
:r .\Scripts\BooksContents.sql
:r .\Scripts\BooksContentLinks.sql