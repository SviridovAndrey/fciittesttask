﻿CREATE TABLE [dbo].[BookAuthors]
(
	[ID] INT IDENTITY(1, 1) NOT NULL,
	[BookID]	INT	NOT NULL,
	[AuthorID]	INT	NOT NULL,
	[Order]	INT	NOT NULL DEFAULT 1,
	CONSTRAINT [PK_BookAuthors] PRIMARY KEY CLUSTERED ([ID] ASC),	
	CONSTRAINT [FK_BookAuthors_Books] FOREIGN KEY ([BookID]) REFERENCES [dbo].[Books] ([ID]),
	CONSTRAINT [FK_BookAuthors_Authors] FOREIGN KEY ([AuthorID]) REFERENCES [dbo].[Authors] ([ID])
)
