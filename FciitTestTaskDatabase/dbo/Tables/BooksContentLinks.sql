﻿CREATE TABLE [dbo].[BooksContentLinks]
(
	[ID] INT IDENTITY(1,1) NOT NULL,
	[BookID] INT NOT NULL,
	[BooksContentID] INT NOT NULL,
	[ExtensionID]	INT	NOT NULL,
	CONSTRAINT [PK_BooksContentLinks] PRIMARY KEY CLUSTERED ([ID] ASC),
	CONSTRAINT [FK_BooksContentLinks_Books] FOREIGN KEY ([BookID]) REFERENCES [dbo].[Books] ([ID]),
	CONSTRAINT [FK_BooksContentLinks_BooksContents] FOREIGN KEY ([BooksContentID]) REFERENCES [dbo].[BooksContents] ([ID]),
	CONSTRAINT [FK_BooksContentLinks_Extensions] FOREIGN KEY ([ExtensionID]) REFERENCES [dbo].[Extensions] ([ID])
)