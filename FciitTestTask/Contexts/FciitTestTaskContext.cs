﻿using FciitTestTaskLibrary.Models;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace FciitTestTaskLibrary.Contexts
{
    public class FciitTestTaskContext : DbContext
    {
        public FciitTestTaskContext()
        {
            Database.EnsureCreated();
        }

        public FciitTestTaskContext(DbContextOptions<FciitTestTaskContext> options)
        : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BookAuthor>()
                .HasKey(t => new { t.BookID, t.AuthorID });

            modelBuilder.Entity<BookAuthor>()
                .HasOne(ba => ba.Author)
                .WithMany(a => a.BookAuthors)
                .HasForeignKey(ba => ba.AuthorID);

            modelBuilder.Entity<BookAuthor>()
                .HasOne(ba => ba.Book)
                .WithMany(b => b.BookAuthors)
                .HasForeignKey(ba => ba.BookID);

            //modelBuilder.Entity<StudentCourse>()
            //    .HasKey(t => new { t.StudentId, t.CourseId });

            //modelBuilder.Entity<StudentCourse>()
            //    .HasOne(sc => sc.Student)
            //    .WithMany(s => s.StudentCourses)
            //    .HasForeignKey(sc => sc.StudentId);

            //modelBuilder.Entity<StudentCourse>()
            //    .HasOne(sc => sc.Course)
            //    .WithMany(c => c.StudentCourses)
            //    .HasForeignKey(sc => sc.CourseId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString);
            }

        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<BookAuthor> BookAuthors { get; set; }
        public DbSet<BooksContentLink> BooksContentLinks { get; set; }
        public DbSet<BooksContent> BooksContents { get; set; }
        public DbSet<Extension> Extensions { get; set; }

    }
}