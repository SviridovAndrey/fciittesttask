﻿using Microsoft.EntityFrameworkCore;

namespace FciitTestTaskLibrary.Contexts
{
    public static class CommonContext
    {
        public static DbContext GetContext(ContextEnum context, string connectionName)
        {
            DbContext c = null;
            switch (context)
            {
                case ContextEnum.FciitTestTask:
                    c = new FciitTestTaskContext();
                    break;
                default:
                    c = new FciitTestTaskContext();
                    break;
            }

            return c;
        }
    }
}
