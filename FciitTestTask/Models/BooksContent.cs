﻿using System;

namespace FciitTestTaskLibrary.Models
{
    public class BooksContent
    {
        public Int32 ID { get; set; }        
        public Byte[] Content {get;set;}
    }
}
