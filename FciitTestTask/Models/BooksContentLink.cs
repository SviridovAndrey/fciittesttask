﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FciitTestTaskLibrary.Models
{
    public class BooksContentLink
    {
        public Int32 ID { get; set; }
        public Int32 ExtensionID { get; set; }
        public Extension Extension { get; set; }
        public Int32 BookID { get; set; }
        public Book Book { get; set; }
        public Int32 BooksContentID { get; set; }
        public BooksContent BooksContent { get; set; }
    }
}
