﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FciitTestTaskLibrary.Models
{
    public class BookAuthor
    {        
        [Required]
        public Int32 BookID { get; set; }
        public Book Book { get; set; }
        [Required]
        public Int32 AuthorID { get; set; }
        public Author Author { get; set; }
        public Int32 Order { get; set; }
    }
}
