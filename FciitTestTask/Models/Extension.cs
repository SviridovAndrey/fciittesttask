﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FciitTestTaskLibrary.Models
{
    public class Extension
    {
        [Required]
        public Int32 ID { get; set; }
        public String Ext { get; set; }
    }
}
