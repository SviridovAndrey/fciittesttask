﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FciitTestTaskLibrary.Models
{
    public class Book
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 ID { get; set; }
        [Required]
        public String Title { get; set; }
        public String ISBN { get; set; }

        public List<BookAuthor> BookAuthors { get; set; }
        public List<BooksContentLink> BooksContentLinks { get; set; }

        public Book()
        {
            BookAuthors = new List<BookAuthor>();
            BooksContentLinks = new List<BooksContentLink>();
        }
    }
}
