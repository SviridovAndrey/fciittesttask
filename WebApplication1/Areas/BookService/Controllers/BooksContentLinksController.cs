﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FciitTestTaskLibrary.Contexts;
using FciitTestTaskLibrary.Models;

namespace FciitTestTaskWebApplication.Areas.BookService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksContentLinksController : ControllerBase
    {
        private readonly FciitTestTaskContext _context;

        public BooksContentLinksController(FciitTestTaskContext context)
        {
            _context = context;
        }

        // GET: api/BooksContentLinks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BooksContentLink>>> GetBooksContentLinks()
        {
            return await _context.BooksContentLinks.ToListAsync();
        }

        // GET: api/BooksContentLinks/GetByBook/5
        [HttpGet("GetByBook/{id}")]
        //[ActionName("GetAuthorByBook")]
        public async Task<ActionResult<IEnumerable<BooksContentLink>>> GetBooksContentLinksByBook(int id)
        {
            return await _context.BooksContentLinks.Where(cl => cl.BookID == id).Include(cl => cl.Extension).ToListAsync();
        }

        // GET: api/BooksContentLinks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BooksContentLink>> GetBooksContentLink(int id)
        {
            var booksContentLink = await _context.BooksContentLinks.FindAsync(id);

            if (booksContentLink == null)
            {
                return NotFound();
            }

            return booksContentLink;
        }

        // PUT: api/BooksContentLinks/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBooksContentLink(int id, BooksContentLink booksContentLink)
        {
            if (id != booksContentLink.ID)
            {
                return BadRequest();
            }

            _context.Entry(booksContentLink).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BooksContentLinkExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BooksContentLinks
        [HttpPost]
        public async Task<ActionResult<BooksContentLink>> PostBooksContentLink(BooksContentLink booksContentLink)
        {
            _context.BooksContentLinks.Add(booksContentLink);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBooksContentLink", new { id = booksContentLink.ID }, booksContentLink);
        }

        // DELETE: api/BooksContentLinks/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<BooksContentLink>> DeleteBooksContentLink(int id)
        {
            var booksContentLink = await _context.BooksContentLinks.FindAsync(id);
            if (booksContentLink == null)
            {
                return NotFound();
            }

            _context.BooksContentLinks.Remove(booksContentLink);
            await _context.SaveChangesAsync();

            return booksContentLink;
        }

        private bool BooksContentLinkExists(int id)
        {
            return _context.BooksContentLinks.Any(e => e.ID == id);
        }
    }
}
