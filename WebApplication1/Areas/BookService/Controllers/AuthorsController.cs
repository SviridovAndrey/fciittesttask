﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FciitTestTaskLibrary.Contexts;
using FciitTestTaskLibrary.Models;

namespace FciitTestTaskWebApplication.Areas.BookService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly FciitTestTaskContext _context;

        public AuthorsController(FciitTestTaskContext context)
        {
            _context = context;
        }

        // GET: api/Authors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Author>>> GetAuthors()
        {
            return await _context.Authors.ToListAsync();
        }

        // GET: api/Authors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Author>> GetAuthor(int id)
        {
            var author = await _context.Authors.FindAsync(id);

            if (author == null)
            {
                return NotFound();
            }

            return author;
        }

        // GET: api/Authors/GetAuthorByBook/5        
        [HttpGet("GetAuthorByBook/{id}")]
        [ActionName("GetAuthorByBook")]
        public async Task<ActionResult<IEnumerable<Author>>> GetAuthorByBook(int id = 0)
        {
            //return await _context.Authors.ToListAsync();

            return await _context.Authors.Join(_context.BookAuthors.Where(ba => ba.BookID == id),
                a => a.ID,
                ba => ba.AuthorID,
                (a, ba) => new Author
                {
                    ID = a.ID,
                    LastName = a.LastName,
                    FirstName = a.FirstName,
                    MiddleName = a.MiddleName
                }).ToListAsync();
        }

        // GET: api/Authors/GetAuthorByBook/5        
        [HttpGet("GetNotLinkedAuthors/{id}")]
        [ActionName("GetNotLinkedAuthors")]
        public async Task<ActionResult<IEnumerable<Author>>> GetNotLinkedAuthors(int id = 0)
        {
            var authors = _context.Authors.Join(_context.BookAuthors.Where(ba => ba.BookID == id),
                a => a.ID,
                ba => ba.AuthorID,
                (a, ba) => new Author
                {
                    ID = a.ID,
                    LastName = a.LastName,
                    FirstName = a.FirstName,
                    MiddleName = a.MiddleName
                }).ToList();

            return await _context.Authors.Where(a => !authors.Select(auth => auth.ID).Contains(a.ID)).ToListAsync();
        }

        // PUT: api/Authors/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAuthor(int id, Author author)
        {
            if (id != author.ID)
            {
                return BadRequest();
            }

            _context.Entry(author).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AuthorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Authors
        [HttpPost]
        public async Task<ActionResult<Author>> PostAuthor(Author author)
        {
            _context.Authors.Add(author);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAuthor", new { id = author.ID }, author);
        }

        // DELETE: api/Authors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Author>> DeleteAuthor(int id)
        {
            var author = await _context.Authors.FindAsync(id);
            if (author == null)
            {
                return NotFound();
            }

            _context.Authors.Remove(author);
            await _context.SaveChangesAsync();

            return author;
        }

        private bool AuthorExists(int id)
        {
            return _context.Authors.Any(e => e.ID == id);
        }
    }
}
