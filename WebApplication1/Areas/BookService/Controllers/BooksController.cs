﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FciitTestTaskLibrary.Contexts;
using FciitTestTaskLibrary.Models;
using Microsoft.AspNetCore.Authorization;

namespace FciitTestTaskWebApplication.Areas.BookService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class BooksController : ControllerBase
    {
        private readonly FciitTestTaskContext _context;
        private class BooksFilter { public Int32 ID { get; set; } };
        public BooksController(FciitTestTaskContext context)
        {
            _context = context;
        }

        // GET: api/Books
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Book>>> GetBooks(string title = "", string isbn = "", string author = "")
        {
         List<int> booksIdByAuthor = new List<int>();
            List<int> booksIdByFilter = new List<int>();
            bool isFilter = false;
            if(author.Length > 0)
            {
                //var temp = _context.BookAuthors.Join(_context.Authors.Where(a => a.FirstName.Contains(author) || a.LastName.Contains(author)),
                //    ba => ba.AuthorID,
                //    a => a.ID,
                //    (ba, a) => new BooksFilter
                //    {
                //        ID = ba.BookID
                //    }).Select(bf => bf.ID);
                //foreach(var t in temp)
                //{
                //    booksid.Add(Convert.ToInt32(t));
                //}
                booksIdByAuthor.AddRange(_context.BookAuthors.Join(_context.Authors.Where(a => a.FirstName.Contains(author) || a.LastName.Contains(author)),
                    ba => ba.AuthorID,
                    a => a.ID,
                    (ba, a) => new BooksFilter
                    {
                        ID = ba.BookID
                    }).Select(bf => bf.ID));
                isFilter = true;
            }
            if (title.Length > 0 || isbn.Length > 0) {
                booksIdByFilter.AddRange(_context.Books.Where(b =>
                    (title.Length == 0 || b.Title.Contains(title)) &&
                (isbn.Length == 0 || b.ISBN.Contains(isbn)
                ) && (!isFilter || booksIdByAuthor.Contains(b.ID))).Select(b => b.ID));
                isFilter = true;
            }
            else
            {
                booksIdByFilter = booksIdByAuthor;
            }

            return await _context.Books.Where(b => !isFilter || booksIdByFilter.Contains(b.ID))
            .Include(b => b.BookAuthors)
                .ThenInclude(ba => ba.Author)
            .Include(b => b.BooksContentLinks)
                .ThenInclude(bcl => bcl.Extension)
            .ToListAsync();

            //return await _context.Books.Where(b =>
            //((title.Length <= 3 && isbn.Length <= 3) ||
            //((title.Length > 3 || isbn.Length > 3) &&
            //(
            //    (title.Length == 0 || b.Title.Contains(title)) &&
            //    (isbn.Length == 0 || b.ISBN.Contains(isbn))
            //    ))))
            //.Include(b => b.BookAuthors)
            //    .ThenInclude(ba => ba.Author)
            //.Include(b => b.BooksContentLinks)
            //    .ThenInclude(bcl => bcl.Extension)
            //.ToListAsync();
        }

        // GET: api/Books/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Book>> GetBook(int id)
        {
            var book = await _context.Books.FindAsync(id);

            if (book == null)
            {
                return NotFound();
            }

            return book;
        }

        // PUT: api/Books/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBook(int id, Book book)
        {
            if (id != book.ID)
            {
                return BadRequest();
            }

            _context.Entry(book).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Books
        [HttpPost]
        public async Task<ActionResult<Book>> PostBook(Book book)
        {
            _context.Books.Add(book);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBook", new { id = book.ID }, book);
        }

        // DELETE: api/Books/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Book>> DeleteBook(int id)
        {
            var book = await _context.Books.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }

            _context.Books.Remove(book);
            await _context.SaveChangesAsync();

            return book;
        }

        private bool BookExists(int id)
        {
            return _context.Books.Any(e => e.ID == id);
        }
    }
}
