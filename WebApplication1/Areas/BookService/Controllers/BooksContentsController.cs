﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FciitTestTaskLibrary.Contexts;
using FciitTestTaskLibrary.Models;
using System.IO;

namespace FciitTestTaskWebApplication.Areas.BookService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksContentsController : ControllerBase
    {
        private readonly FciitTestTaskContext _context;

        public BooksContentsController(FciitTestTaskContext context)
        {
            _context = context;
        }

        // GET: api/BooksContents
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BooksContent>>> GetBooksContents()
        {
            return await _context.BooksContents.ToListAsync();
        }

        // GET: api/BooksContents/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BooksContent>> GetBooksContent(int id)
        {
            BooksContentLink booksContentLink = _context.BooksContentLinks.Where(bcl => bcl.BooksContentID == id).FirstOrDefault();

            if (booksContentLink == null)
            { return NotFound(); }

            Book book = await _context.Books.FindAsync(booksContentLink.BookID);
            Extension extension = await _context.Extensions.FindAsync(booksContentLink.ExtensionID);

            if (book == null || extension == null)
            { return NotFound(); }

            BooksContent booksContent = await _context.BooksContents.FindAsync(id);

            if (booksContent == null)
            {
                return NotFound();
            }

            return File(booksContent.Content, string.Format("application/{0}", extension.Ext.Replace(".", "")), string.Format("{0}{1}",book.Title, extension.Ext));
            //return booksContent;
        }

        // PUT: api/BooksContents/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBooksContent(int id, BooksContent booksContent)
        {
            if (id != booksContent.ID)
            {
                return BadRequest();
            }

            _context.Entry(booksContent).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BooksContentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BooksContents
        [HttpPost("UploadFiles")]
        [Consumes("multipart/form-data")]
        //public async Task<ActionResult<BooksContent>> PostBooksContent(List<IFormFile> files)[FromForm] FileInputModel Files
        public async Task<ActionResult<BooksContent>> PostBooksContent([FromForm] FileInputModel files)
        {
            long size = files.Files.Sum(f => f.Length);
            Dictionary<string, Extension> _extensions = _context.Extensions.ToDictionary<Extension, string>(e => e.Ext.ToLower());

            // full path to file in temp location
            var filePath = Path.GetTempFileName();
            string fileExt = string.Empty;

            foreach (var formFile in files.Files)
            {
                fileExt = Path.GetExtension(formFile.FileName).ToLower();
                if (_extensions.ContainsKey(fileExt))
                {
                    if (formFile.Length > 0)
                    {
                        Stream str = formFile.OpenReadStream();
                        BinaryReader br = new BinaryReader(str);
                        BooksContent booksContent;
                        BooksContentLink booksContentLink = _context.BooksContentLinks.Where(bcl => bcl.ExtensionID == _extensions[fileExt].ID && bcl.BookID == files.BookID).FirstOrDefault();
                        if (booksContentLink == null)
                        {
                            booksContent = new BooksContent()
                            {
                                Content = br.ReadBytes((Int32)str.Length)
                            };
                            _context.BooksContents.Add(booksContent);

                            
                            booksContentLink = new BooksContentLink()
                            {
                                ExtensionID = _extensions[fileExt].ID,
                                BookID = files.BookID,
                                BooksContent = booksContent
                            };
                            _context.BooksContentLinks.Add(booksContentLink);
                        }
                        else {
                            booksContent = _context.BooksContents.Find(booksContentLink.BooksContentID);
                            if (booksContentLink == null)
                            {
                                booksContent = new BooksContent
                                {
                                    Content = br.ReadBytes((Int32)str.Length)
                                };
                                _context.BooksContents.Add(booksContent);
                                booksContentLink.BooksContent = booksContent;
                                _context.Entry(booksContentLink).State = EntityState.Modified;
                            }
                            else
                            {
                                booksContent.Content = br.ReadBytes((Int32)str.Length);
                                _context.Entry(booksContent).State = EntityState.Modified;
                            }
                        }
                        await _context.SaveChangesAsync();
                    }
                }
            }
            //return Ok(new { count = files.Files.Count, size, filePath });
            return RedirectToAction("Edit", "Books", new { id = files.BookID });
        }

        // POST: api/BooksContents
        [HttpPost]
        public async Task<ActionResult<BooksContent>> PostBooksContent(BooksContent booksContent)
        {
            _context.BooksContents.Add(booksContent);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBooksContent", new { id = booksContent.ID }, booksContent);
        }

        // DELETE: api/BooksContents/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<BooksContent>> DeleteBooksContent(int id)
        {
            var booksContent = await _context.BooksContents.FindAsync(id);
            if (booksContent == null)
            {
                return NotFound();
            }

            _context.BooksContents.Remove(booksContent);
            await _context.SaveChangesAsync();

            return booksContent;
        }

        private bool BooksContentExists(int id)
        {
            return _context.BooksContents.Any(e => e.ID == id);
        }

        public class FileInputModel
        {
            public List<IFormFile> Files { get; set; }
            public Int32 BookID { get; set; }
        }
    }
}
