﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FciitTestTaskLibrary.Contexts;
using Microsoft.AspNetCore.Mvc;

namespace FciitTestTaskWebApplication.Controllers
{
    public class BooksWithApiController : Controller
    {
        private readonly FciitTestTaskContext _context;

        public BooksWithApiController(FciitTestTaskContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            ViewBag.ExtensionsList = String.Join(", ", _context.Extensions.Select(e => e.Ext));
            return View();
        }
    }
}